# r3-appimage

AppImage packaging template for The Red Robot Radio

# Packaging instructions

- Install [Arch-Deployer](https://github.com/ivan-hc/Arch-Deployer) on Arch Linux
- Clone this repository
- Unpack [r3 repo](https://patrice.asgardius.company/gitea/asgardius/r3) content on r3 folder
- Create an image for python-pygame
- copy template to appimage working directory
- Pack it
- Yout will get The_Red_Robot_Radio$ARCH.AppImage on desktop folder